
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Principal {
	
	static void EscribirFichero(Persona[] p) throws IOException {
		FileWriter fichero = null;
		PrintWriter pw = null;
		
		try {
			fichero = new FileWriter("datos.txt");
			pw = new PrintWriter(fichero);
			
			for(int i=0; i<p.length; i++) {
				pw.println(p[i]);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		fichero.close();
		
		
	}
	
	static void LeerFichero(Persona[] p) throws IOException {
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;
		
		String nombre = "";
		String apellido = "";
		int edad = 0;
		int n = 0;
		
		
		archivo = new File("datos.txt");
		try {
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);
			
			String linea ;
			
			while((linea = br.readLine()) != null) {
				int space = linea.indexOf(" ");
				int ast = linea.indexOf("#");
				nombre = linea.substring(0, space);
				apellido = linea.substring(space + 1, ast);
				edad = Integer.parseInt(linea.substring(ast + 1, linea.length()));
				p[n] = new Persona(nombre, apellido, edad);
				n++;
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		fr.close();
		
//		for(int i=0; i<p.length; i++) {
//			System.out.println(p[i]);
//		}
		
	}
	
	static void EscribirPantalla(Persona[] p) {
		for(int i=0; i<p.length; i++) {
			System.out.println(p[i]);
		}
		
	}
	
	static void Vaciar(Persona[] p) {

		for(int i=0; i<p.length; i++) {
			p[i] = new Persona();
		}
		
	}
	
	public static void Menu() {
		System.out.println("Menu de OPCIONES");
		System.out.println("----------------");
		System.out.println("1. Escribir en el Fichero");
		System.out.println("2. Leer del Fichero");
		System.out.println("3. Mostrar en Consola");
		System.out.println("4. Vaciar Estructura");
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		
//		int opcion = 0;
		
		Persona personas[] = new Persona[5];
		
		personas[0] = new Persona("Cesar", "Fernandez", 21);
		personas[1] = new Persona("Marcos", "Cabornero", 20);
		personas[2] = new Persona("Daniel", "Medrano", 99);
		personas[3] = new Persona("Roberto", "Sol", 6);
		personas[4] = new Persona("Alejandro", "Casta�o", 35);
		
		
		int opcion;

		
		
//		do {
//			
//			Menu();
//			
//			opcion = sc.nextInt();
//
//
//		switch(opcion) {
//		case 1 :
//			EscribirFichero(personas);
//		break;
//		
//		
//		case 2 :
//			LeerFichero(personas);
//		break;
//		
//		case 3 :
//			EscribirPantalla(personas);
//			break;
//			
//		case 4 : 
//			Vaciar(personas);
//			break;
//			
//			default: 
//		
//		
//		}
//		}while(opcion >= 5);
		
		System.out.println(personas[0]);
		

	}

}
