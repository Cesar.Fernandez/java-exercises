
public class Persona {
	
	private String nombre;
	private String pwd;
	
	
	public Persona(String nombre, String pwd) {
		super();
		this.nombre = nombre;
		this.pwd = pwd;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getPwd() {
		return pwd;
	}


	public void setPwd(String pwd) {
		this.pwd = pwd;
	}


	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", pwd=" + pwd + "]";
	}

}
