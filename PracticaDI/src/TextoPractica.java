
public class TextoPractica {
	
	String texto;

	public TextoPractica(String texto) {
		super();
		this.texto = texto;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	@Override
	public String toString() {
		return texto ;
	}
	
	

}
