import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

public class Estadisticas {
//	ICONO
	static final String rutaIMG = "icon.png";
	
//	Fichero ESTADISITCAS
	static final String rutaEST = "estadistics.txt";



	private JFrame Estadisticas;
	static JLabel lblNombreEstats;
	static JLabel lblErroresEstats;
	static JLabel lblTiempoEstats;
	static JLabel lblLeccionEstats;
	
	public void setFrame(JFrame frame) {
		this.Estadisticas = frame;
	}
	
	Menu Menu;

	public Estadisticas(Menu Menu) {
		initialize();
		this.Menu = Menu;
	
	}
	
	
	private void initialize() {
		Estadisticas = new JFrame();
		Estadisticas.setTitle("Estadisticas");
		Estadisticas.setBounds(100, 100, 792, 628);
		Estadisticas.setLocationRelativeTo(null);
		Estadisticas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ImageIcon icon = new ImageIcon(rutaIMG);
		Estadisticas.setIconImage(icon.getImage());
		Estadisticas.getContentPane().setLayout(null);
		
		JButton btnReturnMenu = new JButton("Volver al Menu");
		btnReturnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Menu.getFrame().setVisible(true);
				Estadisticas.dispose();
				
			}
		});
		btnReturnMenu.setBounds(642, 554, 126, 27);
		Estadisticas.getContentPane().add(btnReturnMenu);
		
		JLabel lblEstadisticas = new JLabel("Estadisticas");
		lblEstadisticas.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblEstadisticas.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstadisticas.setBounds(24, 10, 186, 35);
		Estadisticas.getContentPane().add(lblEstadisticas);
		
		JLabel lblUsuario = new JLabel("Usuario : ");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblUsuario.setBounds(93, 105, 150, 42);
		Estadisticas.getContentPane().add(lblUsuario);
		
		JLabel lblErrores = new JLabel("Errores : ");
		lblErrores.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblErrores.setBounds(93, 208, 150, 42);
		Estadisticas.getContentPane().add(lblErrores);
		
		JLabel lblTiempo = new JLabel("Tiempo");
		lblTiempo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblTiempo.setBounds(93, 342, 161, 42);
		Estadisticas.getContentPane().add(lblTiempo);
		
		lblLeccionEstats = new JLabel("");
		lblLeccionEstats.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLeccionEstats.setBounds(93, 457, 161, 42);
		Estadisticas.getContentPane().add(lblLeccionEstats);
		
		lblNombreEstats = new JLabel("");
		lblNombreEstats.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNombreEstats.setBounds(337, 105, 161, 42);
		Estadisticas.getContentPane().add(lblNombreEstats);
		
		lblErroresEstats = new JLabel("");
		lblErroresEstats.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblErroresEstats.setBounds(337, 225, 161, 42);
		Estadisticas.getContentPane().add(lblErroresEstats);
		
		lblTiempoEstats = new JLabel("");
		lblTiempoEstats.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblTiempoEstats.setBounds(337, 342, 161, 35);
		Estadisticas.getContentPane().add(lblTiempoEstats);


		
		

	}
	
	public JFrame getFrame() {
		return Estadisticas;
	}
}
