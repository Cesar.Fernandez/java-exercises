 import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.plaf.ProgressBarUI;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.Frame;
import java.awt.event.ActionListener;
import java.io.*;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JProgressBar;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class Inicio {
	
//	ICONO
	static final String rutaIMG = "icon.png";
	
	static final String rutaUsuarios = "usuariosAutorizados.txt";
	static final String rutaEstaditicas = "estadisticas.txt";
	static final String rutaTextos = "textos.txt";



	private static JFrame Inicio;
	private static JProgressBar progressBar;
	
	private static Timer t;
	private ActionListener ac;
	private int x = 0;
	
	private JButton btnContinuar;
	
	
	public JFrame getFrame() {
		return Inicio;
	}
	
	public void setFrame(JFrame frame) {
		this.Inicio = frame;
	}
	LogIn LogIn = new LogIn(this);
	
	public static void ComprobarArchivos(String archivoEst, String archivoUsers, String archivoTex) {
		
		File archivo =new File(archivoUsers);
		try {
			FileReader fr = new FileReader(archivo);
			
			fr.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "No se encuentra el archivo de los Usuarios");
			t.stop();
			Inicio.dispose();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		File archivo2 = new File(archivoTex);
		
		try {
			FileReader fr2 = new FileReader(archivo2);
			fr2.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "No se encuentra el achivo de los Textos");
			t.stop();
			Inicio.dispose();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		File archivo3 = new File(archivoEst);
		
		try {
			FileReader fr3 = new FileReader(archivo3);
			fr3.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "No se encuentra el archivo de las Estadisticas");
			t.stop();
			Inicio.dispose();
		} catch (IOException e) {
			e.printStackTrace();
		}
		

	}

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					Inicio window = new Inicio();
					window.Inicio.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Inicio() {
		initialize();
	}
	


	private void initialize() {
		Inicio = new JFrame();
		Inicio.setResizable(false);
		Inicio.getContentPane().setBackground(Color.MAGENTA);
		Inicio.setTitle("Inicio");
		Inicio.setBounds(0, 0, 500, 500);
		Inicio.setLocationRelativeTo(null);
		Inicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Inicio.getContentPane().setLayout(null);
		
		ImageIcon icon = new ImageIcon(rutaIMG);
		Inicio.setIconImage(icon.getImage());
		
		ac = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				x = x + 1;
				progressBar.setValue(x);
				if(progressBar.getValue()==100) {
					btnContinuar.setEnabled(true);
					progressBar.setString("Completado");
				}
				ComprobarArchivos(rutaEstaditicas, rutaUsuarios, rutaTextos);
				
			}
		};
		
		t = new Timer(50, ac);
		t.start();


		
		JLabel lblBienvenido = new JLabel("BIENVENIDO");
		lblBienvenido.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblBienvenido.setHorizontalAlignment(SwingConstants.CENTER);
		lblBienvenido.setBounds(129, 88, 215, 131);
		Inicio.getContentPane().add(lblBienvenido);
		
		btnContinuar = new JButton("Continuar");
		btnContinuar.setEnabled(false);

		btnContinuar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent iniciotomenu) {
				LogIn.getFrame().setVisible(true);
				Inicio.dispose();
			}
		});
		btnContinuar.setBounds(159, 365, 149, 47);
		Inicio.getContentPane().add(btnContinuar);
		
		progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setString("Cargando...");

		progressBar.setBounds(102, 229, 278, 36);
		Inicio.getContentPane().add(progressBar);
		
	}
}
