import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.SwingConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.Highlighter.HighlightPainter;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class Teclado {
	
//	TEXTOS
//	ICONO
	static final String rutaIMG = "icon.png";	
	static final String rutaEsta = "estadisticas.txt";


	private JFrame Teclado;
	JTextPane textPane;
	private JLabel lblCronometro;
	private JLabel lblErrores;
	private JPanel panelBotones;
	static JLabel lblResultado;
	JButton btnExit = new JButton();
	public int contadorErrores = 0;
	public JLabel lblLeccion;
	
	static int hora=0, minuto=0, segundos=0;
	
	static boolean iniciaHilo=true;
	boolean corriendo= false;
	
	public void setFrame(JFrame frame) {
		this.Teclado = frame;
	}
	
	Menu Menu;
	
	protected void teclas(KeyEvent e) {
		JButton aux = null;
		Component[] prueba = panelBotones.getComponents();
		
		for(int i=0; i<prueba.length; i++) {
			aux = (JButton)prueba[i];
			aux.setBackground(Color.WHITE);
		}
		
	}
	
	protected void resaltar(KeyEvent e,int p) {

		JButton encontrado = null;
		JButton aux = null;
		boolean comp = false;
		String cadena = "";
		
		cadena = textPane.getText();
		
		char[] chars = cadena.toCharArray();
		
		
		Component [] prueba = panelBotones.getComponents();
		
		Highlighter fondo = textPane.getHighlighter();
		HighlightPainter verde = new DefaultHighlighter.DefaultHighlightPainter(Color.GREEN);
		HighlightPainter rojo = new DefaultHighlighter.DefaultHighlightPainter(Color.RED);
		

		for(int i=0; i<prueba.length; i++) {
			aux = (JButton) prueba[i];
			if((aux.getText()).toCharArray()[0]==e.getKeyChar()) {
				encontrado=aux;
				i=prueba.length;
				if(aux.getText().toCharArray()[0] == chars[p]) {
					encontrado.setBackground(Color.green);
				}else {
					encontrado.setBackground(Color.red);
				}
		}
		}
	}
	

	
	public void comparar(KeyEvent e, int p) {
	String cadena = "";
	
	JButton encontrado = null;
	JButton aux = null;
	
	Component [] prueba = panelBotones.getComponents();
	
	cadena = textPane.getText();

	char[] chars = cadena.toCharArray();
	
	Highlighter fondo = textPane.getHighlighter();
	HighlightPainter verde = new DefaultHighlighter.DefaultHighlightPainter(Color.GREEN);
	HighlightPainter rojo = new DefaultHighlighter.DefaultHighlightPainter(Color.RED);
	
	
		if(chars[p]==e.getKeyChar()) {
			try {
				fondo.addHighlight(p, p + 1, verde);
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}else {
			try {
				fondo.addHighlight(p, p + 1, rojo);
				contadorErrores++;
				lblErrores.setText(String.valueOf(contadorErrores));
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}
	
	
}
	
	public void Escribir() {
		FileWriter fichero = null;
		PrintWriter pw = null;
		
		try {
			fichero = new FileWriter(rutaEsta,true);
			
			pw = new PrintWriter(fichero);
			
			pw.println("Usuario : " + lblResultado.getText());
			pw.println("Errores : " + lblErrores.getText());
			pw.println("Tiempo :" + lblCronometro.getText());
			pw.println(lblLeccion.getText());
			pw.println();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			fichero.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	
	public void Estats() {
		Estadisticas.lblNombreEstats.setText(lblResultado.getText());
		Estadisticas.lblErroresEstats.setText(lblErrores.getText());
		Estadisticas.lblTiempoEstats.setText(lblCronometro.getText());
		Estadisticas.lblLeccionEstats.setText(lblLeccion.getText());
	}
	

	public Teclado(Menu Menu) {
		initialize();
		this.Menu = Menu;
	}

	
	private void initialize() {
		Teclado = new JFrame();
		Teclado.setResizable(false);
		Teclado.getContentPane().setBackground(Color.ORANGE);
		Teclado.getContentPane().setForeground(Color.WHITE);
		Teclado.setTitle("Teclado");
		Teclado.setBounds(400, 30, 800, 800);
		Teclado.setLocationRelativeTo(null);
		Teclado.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Teclado.getContentPane().setLayout(null);
		Teclado.setFocusable(true);
		
		
		ImageIcon icon = new ImageIcon(rutaIMG);
		
		Teclado.setIconImage(icon.getImage());
		
		JLabel lblPulsacionesTotales = new JLabel("Pulsaciones Totales");
		lblPulsacionesTotales.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblPulsacionesTotales.setBounds(20, 263, 187, 77);
		Teclado.getContentPane().add(lblPulsacionesTotales);
		
		JLabel lblNewLabel = new JLabel("Errores");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(315, 274, 113, 48);
		Teclado.getContentPane().add(lblNewLabel);
		
		JLabel lblTiempo = new JLabel("Tiempo");
		lblTiempo.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblTiempo.setBounds(574, 280, 101, 42);
		Teclado.getContentPane().add(lblTiempo);
		
		panelBotones = new JPanel();
		panelBotones.setBackground(Color.ORANGE);
		panelBotones.setBounds(20, 446, 764, 246);
		Teclado.getContentPane().add(panelBotones);
		panelBotones.setFocusable(true);
		panelBotones.setLayout(null);
		
		JButton button_11 = new JButton("\u00AA \\");
		button_11.setEnabled(false);
		button_11.setBounds(10, 43, 70, 28);
		panelBotones.add(button_11);
		button_11.setPreferredSize(new Dimension(60, 28));
		
		JButton botonUNO = new JButton("1");
		botonUNO.setEnabled(false);
		botonUNO.setPreferredSize(new Dimension(62, 28));
		botonUNO.setBounds(90, 43, 50, 28);
		panelBotones.add(botonUNO);
		
		JButton botonDOS = new JButton("2");
		botonDOS.setEnabled(false);
		botonDOS.setBounds(150, 43, 50, 28);
		panelBotones.add(botonDOS);
		
		JButton botonTRES = new JButton("3");
		botonTRES.setEnabled(false);
		botonTRES.setBounds(210, 43, 50, 28);
		panelBotones.add(botonTRES);
		
		JButton botonCUATRO = new JButton("4");
		botonCUATRO.setEnabled(false);
		botonCUATRO.setBounds(270, 43, 50, 28);
		panelBotones.add(botonCUATRO);
		
		JButton botonCINCO = new JButton("5");
		botonCINCO.setEnabled(false);
		botonCINCO.setBounds(330, 43, 50, 28);
		panelBotones.add(botonCINCO);
		
		JButton botonSEIS = new JButton("6");
		botonSEIS.setEnabled(false);
		botonSEIS.setBounds(390, 43, 50, 28);
		panelBotones.add(botonSEIS);
		
		JButton botonSIETE = new JButton("7");
		botonSIETE.setEnabled(false);
		botonSIETE.setBounds(450, 43, 50, 28);
		panelBotones.add(botonSIETE);
		
		JButton botonOCHO = new JButton("8");
		botonOCHO.setEnabled(false);
		botonOCHO.setBounds(510, 43, 50, 28);
		panelBotones.add(botonOCHO);
		
		JButton botonNUEVE = new JButton("9");
		botonNUEVE.setEnabled(false);
		botonNUEVE.setBounds(570, 43, 50, 28);
		panelBotones.add(botonNUEVE);
		
		JButton botonCERO = new JButton("0");
		botonCERO.setEnabled(false);
		botonCERO.setBounds(630, 43, 50, 28);
		panelBotones.add(botonCERO);
		
		JButton botonQ = new JButton("q");
		botonQ.setBounds(90, 81, 50, 28);
		panelBotones.add(botonQ);
		botonQ.setEnabled(false);
		
		JButton button_12 = new JButton("^");
		button_12.setEnabled(false);
		button_12.setBounds(690, 157, 60, 28);
		panelBotones.add(button_12);
		
		JButton btnNewButton_1 = new JButton("<--");
		btnNewButton_1.setBounds(690, 43, 60, 28);
		panelBotones.add(btnNewButton_1);
		btnNewButton_1.setEnabled(false);
		
		JButton button_10 = new JButton("<--");
		button_10.setEnabled(false);
		button_10.setBounds(10, 81, 70, 28);
		panelBotones.add(button_10);
		
		JButton botonW = new JButton("w");
		botonW.setBounds(150, 81, 50, 28);
		panelBotones.add(botonW);
		botonW.setEnabled(false);
		
		JButton btnE = new JButton("e");
		btnE.setBounds(210, 81, 50, 28);
		panelBotones.add(btnE);
		btnE.setEnabled(false);
		
		JButton botonR = new JButton("r");
		botonR.setBounds(270, 81, 50, 28);
		panelBotones.add(botonR);
		botonR.setEnabled(false);
		
		JButton botonT = new JButton("t");
		botonT.setBounds(330, 81, 50, 28);
		panelBotones.add(botonT);
		botonT.setEnabled(false);
		
		JButton botonY = new JButton("y");
		botonY.setEnabled(false);
		botonY.setBounds(390, 81, 50, 28);
		panelBotones.add(botonY);
		
		JButton botonU = new JButton("u");
		botonU.setEnabled(false);
		botonU.setBounds(450, 81, 50, 28);
		panelBotones.add(botonU);
		
		JButton botonI = new JButton("i");
		botonI.setEnabled(false);
		botonI.setBounds(510, 81, 50, 28);
		panelBotones.add(botonI);
		
		JButton botonO = new JButton("o");
		botonO.setEnabled(false);
		botonO.setBounds(570, 81, 50, 28);
		panelBotones.add(botonO);
		
		JButton botonP = new JButton("p");
		botonP.setEnabled(false);
		botonP.setBounds(630, 81, 50, 28);
		panelBotones.add(botonP);
		
		JButton botonEnter = new JButton("^");
		botonEnter.setEnabled(false);
		botonEnter.setBounds(690, 81, 60, 65);
		panelBotones.add(botonEnter);
		
		JButton btnBloq = new JButton("Bloq ");
		btnBloq.setEnabled(false);
		btnBloq.setBounds(10, 119, 70, 28);
		panelBotones.add(btnBloq);
		
		JButton button_4 = new JButton("^");
		button_4.setEnabled(false);
		button_4.setBounds(10, 157, 70, 28);
		panelBotones.add(button_4);
		
		JButton btnControl_1 = new JButton("Ctrl");
		btnControl_1.setEnabled(false);
		btnControl_1.setBounds(10, 195, 70, 28);
		panelBotones.add(btnControl_1);
		
		JButton botonA = new JButton("a");
		botonA.setEnabled(false);
		botonA.setBounds(90, 119, 50, 28);
		panelBotones.add(botonA);
		
		JButton botonS = new JButton("s");
		botonS.setEnabled(false);
		botonS.setBounds(150, 119, 50, 28);
		panelBotones.add(botonS);
		
		JButton botonD = new JButton("d");
		botonD.setEnabled(false);
		botonD.setBounds(210, 119, 50, 28);
		panelBotones.add(botonD);
		
		JButton botonF = new JButton("f");
		botonF.setEnabled(false);
		botonF.setBounds(270, 119, 50, 28);
		panelBotones.add(botonF);
		
		JButton botonG = new JButton("g");
		botonG.setEnabled(false);
		botonG.setBounds(330, 119, 50, 28);
		panelBotones.add(botonG);
		
		JButton botonH = new JButton("h");
		botonH.setEnabled(false);
		botonH.setBounds(390, 119, 50, 28);
		panelBotones.add(botonH);
		
		JButton botonJ = new JButton("j");
		botonJ.setEnabled(false);
		botonJ.setBounds(450, 119, 50, 28);
		panelBotones.add(botonJ);
		
		JButton botonK = new JButton("k");
		botonK.setEnabled(false);
		botonK.setBounds(510, 119, 50, 28);
		panelBotones.add(botonK);
		
		JButton botonL = new JButton("l");
		botonL.setEnabled(false);
		botonL.setBounds(570, 119, 50, 28);
		panelBotones.add(botonL);
		
		JButton boton� = new JButton("\u00F1");
		boton�.setEnabled(false);
		boton�.setBounds(630, 119, 50, 28);
		panelBotones.add(boton�);
		
		JButton botonZ = new JButton("z");
		botonZ.setEnabled(false);
		botonZ.setBounds(90, 157, 50, 28);
		panelBotones.add(botonZ);
		
		JButton botonX = new JButton("x");
		botonX.setEnabled(false);
		botonX.setBounds(150, 157, 50, 28);
		panelBotones.add(botonX);
		
		JButton botonC = new JButton("c");
		botonC.setEnabled(false);
		botonC.setBounds(210, 157, 50, 28);
		panelBotones.add(botonC);
		
		JButton botonV = new JButton("v");
		botonV.setEnabled(false);
		botonV.setBounds(270, 157, 50, 28);
		panelBotones.add(botonV);
		
		JButton botonB = new JButton("b");
		botonB.setEnabled(false);
		botonB.setBounds(330, 157, 50, 28);
		panelBotones.add(botonB);
		
		JButton botonN = new JButton("n");
		botonN.setEnabled(false);
		botonN.setBounds(390, 157, 50, 28);
		panelBotones.add(botonN);
		
		JButton botonM = new JButton("m");
		botonM.setEnabled(false);
		botonM.setBounds(450, 157, 50, 28);
		panelBotones.add(botonM);
		
		JButton btnK_1 = new JButton(";");
		btnK_1.setEnabled(false);
		btnK_1.setBounds(510, 157, 50, 28);
		panelBotones.add(btnK_1);
		
		JButton button_23 = new JButton(":");
		button_23.setEnabled(false);
		button_23.setBounds(570, 157, 50, 28);
		panelBotones.add(button_23);
		
		JButton button_17 = new JButton("-");
		button_17.setEnabled(false);
		button_17.setBounds(630, 157, 50, 28);
		panelBotones.add(button_17);
		button_17.setPreferredSize(new Dimension(50, 25));
		
		JButton btnW_2 = new JButton("W");
		btnW_2.setEnabled(false);
		btnW_2.setBounds(90, 195, 60, 28);
		panelBotones.add(btnW_2);
		btnW_2.setPreferredSize(new Dimension(50, 25));
		btnW_2.setMaximumSize(new Dimension(50, 25));
		
		JButton btnAlt = new JButton("Alt");
		btnAlt.setEnabled(false);
		btnAlt.setBounds(160, 195, 60, 28);
		panelBotones.add(btnAlt);
		
		JButton btnControl = new JButton("Ctrl");
		btnControl.setEnabled(false);
		btnControl.setBounds(684, 195, 70, 28);
		panelBotones.add(btnControl);
		
		JButton button_24 = new JButton("-");
		button_24.setEnabled(false);
		button_24.setBounds(625, 195, 55, 28);
		panelBotones.add(button_24);
		
		JButton btnW_1 = new JButton("FN");
		btnW_1.setEnabled(false);
		btnW_1.setBounds(560, 195, 60, 28);
		panelBotones.add(btnW_1);
		
		JButton btnAlt_1 = new JButton("AltGr");
		btnAlt_1.setEnabled(false);
		btnAlt_1.setBounds(480, 195, 70, 28);
		panelBotones.add(btnAlt_1);
		
		JButton space = new JButton("-");
		space.setEnabled(false);
		space.setBounds(230, 195, 240, 28);
		panelBotones.add(space);
		space.setPreferredSize(new Dimension(100, 35));
		
		textPane = new JTextPane();
		textPane.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textPane.setEditable(false);
		textPane.setBounds(10, 11, 774, 253);
		Teclado.getContentPane().add(textPane);
		
		JLabel lblPulsaciones = new JLabel("");
		lblPulsaciones.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblPulsaciones.setBounds(47, 357, 94, 30);
		Teclado.getContentPane().add(lblPulsaciones);
		
		lblErrores = new JLabel("0");
		lblErrores.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblErrores.setBounds(315, 333, 137, 37);
		Teclado.getContentPane().add(lblErrores);
		
		lblCronometro = new JLabel("00:00:00");
		lblCronometro.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblCronometro.setHorizontalAlignment(SwingConstants.CENTER);
		lblCronometro.setBounds(541, 332, 147, 48);
		Teclado.getContentPane().add(lblCronometro);
		lblCronometro.setFocusable(true);
		
		btnExit = new JButton("Volver al Menu");
		btnExit.setEnabled(false);
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Menu.getFrame().setVisible(true);
				Teclado.dispose();
				Escribir();
				Estats();
				iniciaHilo=false;
				corriendo=false;
				hora=0;
				segundos=0;
				minuto=0;
				lblCronometro.setText("00:00:00");
				
				
			}
		});
		btnExit.setBounds(645, 735, 107, 25);
		Teclado.getContentPane().add(btnExit);
		
		JLabel lblUsuario = new JLabel("Usuario : ");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUsuario.setBounds(36, 397, 79, 26);
		Teclado.getContentPane().add(lblUsuario);
		
		lblResultado = new JLabel("");
		lblResultado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblResultado.setBounds(107, 402, 101, 17);
		Teclado.getContentPane().add(lblResultado);
		
		lblLeccion = new JLabel("");
		lblLeccion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLeccion.setBounds(302, 381, 126, 41);
		Teclado.getContentPane().add(lblLeccion);
		
		Teclado.addKeyListener(new KeyListener() {
			int contador = 0;
			public void keyTyped(KeyEvent e) {
				resaltar(e, contador);
				comparar(e, contador);
				if(corriendo==false) {
					iniciaHilo= true;
					corriendo = true;
					iniciarHiloCronometro();
					}
			
			contador++;
			lblPulsaciones.setText(String.valueOf(contador));
			if(contador == textPane.getText().length()) {
				btnExit.setEnabled(true);
				corriendo = false;
				iniciaHilo=false;
				Teclado.setFocusable(false);
				
			}
				
			}
			public void keyReleased(KeyEvent e) {
				teclas(e);
			}
			public void keyPressed(KeyEvent e) {
			}
		});
	}
	
	private void iniciarHiloCronometro() {
		if(iniciaHilo==true) {
		Crono miCrono = new Crono(lblCronometro);
		miCrono.start();
		}
		
	}
	
	public JFrame getFrame() {
		return Teclado;
	}
}
