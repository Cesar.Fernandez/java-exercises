import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.sound.sampled.Line;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;

public class LogIn {

//	Archivo Usuarios
	static final String ruta = "usuariosAutorizados.txt";
//	ICOCNO
	static final String rutaIMG = "icon.png";

	

	private JFrame LogIn;
	private JTextField textUsuario;
	Inicio Inicio;
	
	public void setFrame(JFrame frame) {
		this.LogIn = frame;
	}
	
	Menu Menu = new Menu(this);
	private JPasswordField passwordField;
	
	public void abrirVentana() {
		Menu.getFrame().setVisible(true);
		LogIn.dispose();
		
	}
	
	public void comparar(String c, String h) throws IOException {
	String text = "";
	String sName="";
	String sPass = "";
	int n=0;
	int p=0;
	boolean seguir = false;
	Persona personas[] = new Persona[2];
	

	try {
		FileReader f = new FileReader(ruta);
		BufferedReader b = new BufferedReader(f);
		while((text = b.readLine()) != null) {
			int space=text.indexOf(" ");
			sName=text.substring(1, space);
			sPass=text.substring(space + 1, text.length());
			personas[n] = new Persona(sName,sPass);
			n++;
			
		}
		
		while(p<personas.length) {
			if(personas[p].getNombre().equals(c) && personas[p].getPwd().equals(h)) {
				abrirVentana();
				Teclado.lblResultado.setText(c);
				p=personas.length;
				seguir = true;
			}
			p++;
		}
		
		if(seguir==false) {
			JOptionPane.showMessageDialog(null, "Error. Usuario o Contraseņa incorrecta");
		}
		
		
		b.close();
	}
	catch(IOException e){
		e.printStackTrace();
	}
	}
	

	public LogIn(Inicio Inicio) {
		initialize();
		this.Inicio = Inicio;
	}

	
	private void initialize() {
		LogIn = new JFrame();
		LogIn.setResizable(false);
		LogIn.getContentPane().setBackground(new Color(50, 205, 50));
		LogIn.setTitle("Log In");
		LogIn.setBounds(100,100, 500, 500);
		LogIn.setLocationRelativeTo(null);
		LogIn.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		LogIn.getContentPane().setLayout(null);
		
		ImageIcon icon = new ImageIcon(rutaIMG);
		LogIn.setIconImage(icon.getImage());


		
		JLabel lblNombreUsuario = new JLabel("Usuario");
		lblNombreUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombreUsuario.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNombreUsuario.setBackground(Color.WHITE);
		lblNombreUsuario.setBounds(109, 29, 247, 83);
		LogIn.getContentPane().add(lblNombreUsuario);
		
		textUsuario = new JTextField();
		textUsuario.setBounds(140, 123, 200, 40);
		LogIn.getContentPane().add(textUsuario);
		textUsuario.setColumns(10);
		
		JButton btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					comparar(textUsuario.getText(), passwordField.getText());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		btnConfirmar.setBounds(182, 332, 114, 23);
		LogIn.getContentPane().add(btnConfirmar);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblContrasea.setHorizontalAlignment(SwingConstants.CENTER);
		lblContrasea.setBounds(150, 174, 190, 59);
		LogIn.getContentPane().add(lblContrasea);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(140, 244, 200, 40);
		LogIn.getContentPane().add(passwordField);
	}
	
	public JFrame getFrame() {
		return LogIn;
	}
}
