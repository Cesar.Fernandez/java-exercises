
import javax.swing.JLabel;

public class Crono extends Thread{
	
	JLabel eti;

	public Crono(JLabel lblCronometro) {
		this.eti = lblCronometro;
	}
	
	public void run () {
		try {
			int x = 0;
			
			while(Teclado.iniciaHilo) {
				Thread.sleep(1000);
				ejecutarHiloCronometro();
				x++;
				
				
			}
			
		} catch (Exception e) {
			System.out.println("Excepcion en ell hilo " + e.getMessage());
		}
	}

	private void ejecutarHiloCronometro() {
		Teclado.segundos++;
		
		if(Teclado.segundos>59) {
			Teclado.segundos = 0;
			Teclado.minuto++;
			if(Teclado.minuto>59) {
				Teclado.minuto = 0;
				Teclado.hora++;
			}
		}
		
		String textSeg = "", textMin = "", textHo = "";
		
		if(Teclado.segundos<10) {
			textSeg="0"+Teclado.segundos;
		}else {
			textSeg=""+Teclado.segundos;
		}
		
		if(Teclado.minuto<10) {
			textMin="0"+Teclado.minuto;
		}else {
			textSeg=""+Teclado.minuto;
			}
		if(Teclado.hora<10) {
			textHo="0"+Teclado.hora;
		}else {
			textSeg=""+Teclado.hora;
		}
		
			
		
		String reloj = textHo+":"+textMin+":"+textSeg;
		
		
		eti.setText(reloj);
		}


}