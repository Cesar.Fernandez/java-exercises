import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Menu {
	
//	ICONO
	static final String rutaIMG = "icon.png";
	static final String ruta = "textos.txt";




	private JFrame Menu;
	LogIn LogIn;
	
	public void setFrame(JFrame frame) {
		this.Menu = frame;
	}
	
	Teclado Teclado = new Teclado(this);
	Estadisticas Estadisticas = new Estadisticas(this);
	
	TextoPractica lineas[] = new TextoPractica[2];
	
	public void leerTexto() throws IOException {
	String text = "";
	String sTexto="";
	int n=0;
	int p=0;
	
	
	

	try {
		FileReader f = new FileReader(ruta);
		BufferedReader b = new BufferedReader(f);
		while((text = b.readLine()) != null) {
			sTexto=text.substring(0 , text.length());
			lineas[n] = new TextoPractica(sTexto);
			n++;
			
		}
		
		
		b.close();
	}
	catch(IOException e){
		e.printStackTrace();
	}
	}
	
	public void mostrarTexto1() throws FileNotFoundException , IOException {
		String cadena = "";
		
		cadena = String.valueOf(lineas[0]);
		
		Teclado.textPane.setText(cadena);
		Teclado.lblLeccion.setText("Leccion 1");
	}
	
	public void mostrarTexto2() throws FileNotFoundException , IOException {
		String cadena = "";
		
		cadena = String.valueOf(lineas[1]);
		
		Teclado.textPane.setText(cadena);
		Teclado.lblLeccion.setText("Leccion 2");
	}

	


	public Menu(LogIn LogIn) {
		initialize();
		this.LogIn = LogIn;
	}
	
	
	private void initialize() {
		Menu = new JFrame();
		Menu.setResizable(false);
		Menu.getContentPane().setBackground(Color.YELLOW);
		Menu.setTitle("Menu");
		Menu.setBounds(100, 100, 500, 500);
		Menu.setLocationRelativeTo(null);
		Menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Menu.getContentPane().setLayout(null);
		
		ImageIcon icon = new ImageIcon(rutaIMG);
		Menu.setIconImage(icon.getImage());


		
		JButton btnNewButton = new JButton("Lecci\u00F3n 1");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent menutoL1) {
				Teclado.getFrame().setVisible(true);
				Menu.dispose();
				
				try {
					leerTexto();
					mostrarTexto1();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(153, 126, 162, 52);
		Menu.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Lecci\u00F3n 2");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent menutoL2) {
				Teclado.getFrame().setVisible(true);
				Menu.dispose();
				try {
					leerTexto();
					mostrarTexto2();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(153, 224, 162, 52);
		Menu.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Ver Estad\u00EDsticas");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent menutostats) {
				Estadisticas.getFrame().setVisible(true);
				Menu.dispose();
			}
		});
		btnNewButton_2.setBounds(127, 328, 216, 61);
		Menu.getContentPane().add(btnNewButton_2);
	}
	
	public JFrame getFrame() {
		return Menu;
	}
	

}
